# scalab-file-service 
Micro Service de envoi fichier d'un repertoir source vers un autre repertoir  (JAVA)

## Installation

* Télécharger et dézipper les sources, ou cloner le projet : 
```  $ git clone http://archiforge/scalab/scalab-file-service.git ```
* cd dans ``` scalab-profil-service ```


## Test de l'application

## Test via un client REST 


 Url et Object à envoyer: un Dto FichierDto qui a 3 champs fichierSource, repertoireDestination et le nom du  fichier destination
 ``` Remarque: fichierSource, nomFichierDestination et repertoireDestination doit correspondre aux noms des champs de FichierDto. et l'extension du fichier de destination sera ajouté par l'appli  ```
 
 a-Cas envoi
 
 * http://localhost:8080/app/envoyer
 
  ```javascript
  {
    "fichierSource": "C:/DossierSource/fichierAenvoyer.jpg",
    "nomFichierDestination": "nomfichier",
    "repertoireDestination": "E:/DossierDestination"
  }
```
 b-Cas suppréssion
 
 * http://localhost:8080/app/supprimer
 
  ```javascript
  {
    "nomFichierDestination": "nomfichier.jpg",
    "repertoireDestination": "E:/DossierDestination"
  }
```
## Test en utilisant curl à télécharger et installer

```1- depuis l'invité de commande windows ```

  ```javascript
curl -H "Content-Type: application/json" -X POST 
 -d {\"fichierSource\":\"C:/test/fichierAenvoyer.jpg\",\"fichierDestination\":\"E:/DossierDestination\","nomFichierDestination"}  http://localhost:8080/app/envoyer
```

```2- depuis un terminal ```

  ```javascript
 curl -H "Content-Type: application/json" -X POST 
-d '{"fichierSource":"C:/DossierSource/fichierAenvoyer.jpg","fichierDestination":"E:/DossierDestination","nomFichierDestination"}'  http://localhost:8080/app/envoyer
```


 
## Contributeurs

- Laurent KERHERVE (laurent.kerherve@scalian.com)
- Mamadou Oury SOW (mamadououry.sow@scalian.com)